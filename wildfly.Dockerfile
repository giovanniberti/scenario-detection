FROM jboss/wildfly

USER root

COPY ./tsung/container_key.pub /container_key.pub

RUN chmod 600 /container_key.pub && mkdir -p ~/.ssh && cat /container_key.pub >> ~/.ssh/authorized_keys && \ 
 yum install -y wget openssh-server openssh-clients net-tools epel-release && \
 wget https://dev.mysql.com/get/mysql80-community-release-el7-1.noarch.rpm --content-disposition && \
 ls && \
 rpm -Uvh mysql80-community-release-el7-1.noarch.rpm && \
 yum install -y mysql-connector-java mysql munin-node erlang net-snmp net-snmp-utils && \
 echo "allow *" >> /etc/munin/munin-node.conf && \
 mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.orig && \
 echo -e "\n\
com2sec Mybox     localhost          public \n\
com2sec cacti     default     public \n\
#      group.name sec.model  sec.name \n\
group   RWGroup    v2c       Mybox \n\
group   ROGroup    v1        cacti \n\
group   ROGroup    v2c       cacti \n\
view all     included  .1        80 \n\
view system  included  system    fe \n\
#              context sec.model sec.level prefix  read    write  notif \n\
access  ROGroup   \"\"      any    noauth    exact   all     none   none \n\
access  RWGroup   \"\"      v2c    noauth    exact   all     all    all \n\
" > /etc/snmp/snmpd.conf && \
 mkdir -p /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/ && \
 cp /usr/share/java/mysql-connector-java.jar /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/ && \
 echo '<module xmlns="urn:jboss:module:1.5" name="com.mysql"> \
    <resources> \
        <resource-root path="mysql-connector-java.jar" /> \
    </resources> \
    <dependencies> \
        <module name="javax.api"/> \
        <module name="javax.transaction.api"/> \
    </dependencies> \
</module>' > /opt/jboss/wildfly/modules/system/layers/base/com/mysql/main/module.xml && \
 cp /opt/jboss/wildfly/standalone/configuration/standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone_old.xml && \
 echo -e "$(head -n 165 /opt/jboss/wildfly/standalone/configuration/standalone_old.xml)\n$(echo -n "<driver name=\"mysql\" module=\"com.mysql\">\n \
 <driver-class>com.mysql.jdbc.Driver</driver-class>\n \
</driver>")\n$(tail -n +166 /opt/jboss/wildfly/standalone/configuration/standalone_old.xml)" > /opt/jboss/wildfly/standalone/configuration/standalone.xml && \
 /opt/jboss/wildfly/bin/add-user.sh admin password --silent && \
 ssh-keygen -A 

CMD /usr/sbin/sshd && snmpd && /opt/jboss/wildfly/bin/standalone.sh --server-config=standalone.xml -bmanagement=0.0.0.0 -b 0.0.0.0
