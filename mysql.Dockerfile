FROM mysql

ADD init.sql /docker-entrypoint-initdb.d

USER root
RUN echo "[mysqld]\n\
default-authentication-plugin=mysql_native_password\n\
collation-server = utf8_unicode_ci\n\
character-set-server = utf8\n\
skip-character-set-client-handshake" > /etc/mysql/conf.d/my.cnf
USER mysql
