package it.unifi.webapp.backend.model;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class UserScenario implements Serializable {

	@Expose
	@ManyToOne
	private User user;
	@Expose
	private String scenario;

	public UserScenario() {}

	public UserScenario(User u, String s){
		this.user = u;
		this.scenario = s;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}
}
