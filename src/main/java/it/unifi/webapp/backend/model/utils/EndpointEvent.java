package it.unifi.webapp.backend.model.utils;

import java.util.Map;

public class EndpointEvent {

	private String useCaseType;
	private long timestamp;
	private int eventType;
	private Map<String, String> eventData;

	public EndpointEvent() {
	}

	public EndpointEvent(String useCaseType, long timestamp, int eventType, Map<String, String> eventData) {
		this.useCaseType = useCaseType;
		this.timestamp = timestamp;
		this.eventType = eventType;
		this.eventData = eventData;
	}

	public int getEventType() {
		return eventType;
	}

	public String getUseCaseType() {
		return useCaseType;
	}

	public void setUseCaseType(String useCaseType) {
		this.useCaseType = useCaseType;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Map<String, String> getEventData() {
		return eventData;
	}

	@Override
	public String toString() {
		return "Logging event: " + this.useCaseType + " - time:" + timestamp + " - eventData: " + eventData;
	}
}
