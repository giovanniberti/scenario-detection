package it.unifi.webapp.backend.model.utils;

public enum ObservedEvents {

	DELETE_VIDEO(0),
	SEARCH(1),
	VIEW_VIDEO(2),
	VIEW_CHANNEL(3),
	LOGIN(4),
	LIKE(5),
	COMMENT(6),
	SUBSCRIBE(7),
	LOAD(8),
	LOGOUT(9),
	DISLIKE(10),
	UNSUBSCRIBE(11),
    RATING(12),
	STATS(13);

	public final int eventNumber;

	ObservedEvents(int eventNumber) {
		this.eventNumber = eventNumber;
	}
}
