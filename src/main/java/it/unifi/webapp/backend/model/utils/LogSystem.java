package it.unifi.webapp.backend.model.utils;

import it.unifi.webapp.backend.dao.ScenarioCounterDao;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class LogSystem {

	private static List<SessionScenario> ssList = new ArrayList<>();
	@Inject
	private ScenarioCounterDao scenarioCounterDao;

	public SessionScenario getSessionScenario(String name, int id) {
		for (SessionScenario ss : ssList) {
			if (ss.getScenario().equals(name) && ss.getId() == id) {
				return ss;
			}
		}
		return null;
	}

	public void addSessionScenario(SessionScenario ss) {
		ssList.add(ss);
		System.out.println("Actual number of active scenarios: " + ssList.size());
	}

	public boolean endScenario(String scenario, int id) {
		SessionScenario ss = this.getSessionScenario(scenario, id);
		if (ss != null && ss.getEndpointEvent(0) != null) {
			ScenarioCounter scenarioCounter = scenarioCounterDao.find(ss.getUser(), ss.getScenario());
			if (scenarioCounter != null) {
				scenarioCounter.done();
				scenarioCounterDao.update(scenarioCounter);
			} else {
				System.out.println("An user ends a scenario for the first time.");
				scenarioCounter = new ScenarioCounter(ss.getUser(), ss.getScenario());
				scenarioCounterDao.update(scenarioCounter);
			}
			ss.writeFile();
			ssList.remove(ss);
			return true;
		}
		return false;
	}

	public void log(String scenario, int id, ObservedEvent event) {
		SessionScenario ss = this.getSessionScenario(scenario, id);
		if (ss != null) {
			long timeS = ss.getStartTime();
			EndpointEvent ee = new EndpointEvent(event.eventType.name(), System.currentTimeMillis() - timeS, event.eventType.eventNumber, event.additionalParameters);
			System.out.println(ee.toString());
			this.getSessionScenario(scenario, id).addEndpointEvent(ee);
		} else {
			System.out.println("Event: " + event.eventType.name() + " but no valid scenario started for id: " + id + ", this " +
                                       "call will not be logged");
		}
	}
}
