package it.unifi.webapp.backend.model.utils;

import com.google.gson.annotations.Expose;
import it.unifi.webapp.backend.model.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "Riddle")
public class Riddle extends BaseEntity {

	@Expose
	private String riddle;
	@Expose
	private String solution;

	private Timestamp commitTime;

	public Riddle() {}

	public Riddle(String riddle, String solution) {
		super(UUID.randomUUID().toString());
		this.riddle = riddle;
		this.solution = solution;
		this.commitTime = new Timestamp(System.currentTimeMillis());
	}

	public String getRiddle() {
		return riddle;
	}

	public void setRiddle(String riddle) {
		this.riddle = riddle;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public Timestamp getCommitTime() {
		return commitTime;
	}

	public void setCommitTime(Timestamp commitTime) {
		this.commitTime = commitTime;
	}
}
