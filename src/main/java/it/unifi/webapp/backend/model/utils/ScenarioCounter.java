package it.unifi.webapp.backend.model.utils;

import com.google.gson.annotations.Expose;
import it.unifi.webapp.backend.model.User;
import it.unifi.webapp.backend.model.UserScenario;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ScenarioCounter implements Serializable {

	@Expose
	@EmbeddedId
	private UserScenario userScenario;

	@Expose
	private Long times;

	protected ScenarioCounter(){}

	public ScenarioCounter(User u, String s){
		this.userScenario = new UserScenario(u,s);
		this.times = 1l;
	}

	public void done(){
		this.times += 1;
	}

	public Long getTimes() {
		return times;
	}

	public void setTimes(Long times) {
		this.times = times;
	}

	public UserScenario getUserScenario() {
		return userScenario;
	}

	public void setUserScenario(UserScenario userScenario) {
		this.userScenario = userScenario;
	}

}
