package it.unifi.webapp.backend.model.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ObservedEvent {
    public final ObservedEvents eventType;
    public final Map<String, String> additionalParameters;

    public ObservedEvent(ObservedEvents eventType, Map<String, String> additionalParameters) {
        this.eventType = eventType;
        this.additionalParameters = additionalParameters;
    }

    public static ObservedEvent deleteVideo() { return new ObservedEvent(ObservedEvents.DELETE_VIDEO, Collections.emptyMap()); }

    public static ObservedEvent search() { return new ObservedEvent(ObservedEvents.SEARCH, Collections.emptyMap()); }

    public static ObservedEvent viewVideo() { return new ObservedEvent(ObservedEvents.VIEW_VIDEO, Collections.emptyMap()); }

    public static ObservedEvent viewChannel() { return new ObservedEvent(ObservedEvents.VIEW_CHANNEL, Collections.emptyMap()); }

    public static ObservedEvent login() { return new ObservedEvent(ObservedEvents.LOGIN, Collections.emptyMap()); }

    public static ObservedEvent like() { return new ObservedEvent(ObservedEvents.LIKE, Collections.emptyMap()); }

    public static ObservedEvent comment() { return new ObservedEvent(ObservedEvents.COMMENT, Collections.emptyMap()); }

    public static ObservedEvent subscribe() { return new ObservedEvent(ObservedEvents.SUBSCRIBE, Collections.emptyMap()); }

    public static ObservedEvent load() { return new ObservedEvent(ObservedEvents.LOAD, Collections.emptyMap()); }

    public static ObservedEvent logout() { return new ObservedEvent(ObservedEvents.LOGOUT, Collections.emptyMap()); }

    public static ObservedEvent dislike() { return new ObservedEvent(ObservedEvents.DISLIKE, Collections.emptyMap()); }

    public static ObservedEvent rating(int rating) {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("rating", String.valueOf(rating));
        return new ObservedEvent(ObservedEvents.RATING, attributes);
    }

    public static ObservedEvent unsubscribe() { return new ObservedEvent(ObservedEvents.UNSUBSCRIBE, Collections.emptyMap()); }

    public static ObservedEvent stats() { return new ObservedEvent(ObservedEvents.STATS, Collections.emptyMap()); }
}
