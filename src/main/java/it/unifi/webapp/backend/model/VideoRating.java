package it.unifi.webapp.backend.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "VideoRating")
public class VideoRating extends BaseEntity {

    private static int MAX_RATING = 100;

    private Timestamp commitTime;
    private int rating;

    @OneToOne
    private User rater;

    @OneToOne
    private Video video;

    public VideoRating() {
    }

    public VideoRating(String uuid, User rater, Video video, int rating) {
        super(uuid);
        this.rater = rater;
        this.video = video;
        this.commitTime = new Timestamp(System.currentTimeMillis());

        if (rating < 0) {
            rating = 0;
        } else if (rating > MAX_RATING) {
            rating = MAX_RATING;
        }

        this.rating = rating;
    }

    public Timestamp getCommitTime() {
        return commitTime;
    }

    public int getRating() {
        return rating;
    }

    public Video getVideo() {
        return video;
    }
}
