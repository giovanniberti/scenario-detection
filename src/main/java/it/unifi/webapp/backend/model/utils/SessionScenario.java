package it.unifi.webapp.backend.model.utils;

import it.unifi.webapp.backend.model.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionScenario {

	private List<EndpointEvent> eeList;
	private long startTime;
	LocalDateTime startLocalTime;
	LocalDateTime endLocalTime;
	private String scenario;
	private int id;
	private User user;

	public SessionScenario(String scenario, int id, User user) {
		eeList = new ArrayList<>();
		startTime = System.currentTimeMillis();
		startLocalTime = LocalDateTime.now();
		this.scenario = scenario;
		this.id = id;
		this.user = user;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getStartTime() {
		return startTime;
	}

	public EndpointEvent getEndpointEvent(int index) {
		if (index < eeList.size())
			return eeList.get(index);
		return null;
	}

	public void addEndpointEvent(EndpointEvent ee) {
		eeList.add(ee);
	}

	public void writeFile() {
		this.endLocalTime = LocalDateTime.now();
		String filename = "/opt/scenedet-results/" + scenario + ".csv";
		System.out.println("Appending sequence to file " + filename);
		writeFile(filename,this.toString());
	}

	private static synchronized void writeFile(String fileName, String sessionScenario){
		try {
			FileWriter fw = new FileWriter(fileName, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			out.println(sessionScenario);
			out.close();
		} catch (IOException e) {
			System.err.println("Unable to write to scenario file: " + e.getMessage());
		}
	}

	@Override
	public String toString() {
		String s = startLocalTime.toString() + "," +
				endLocalTime.toString() + "," +user.getUuid() + "," ;
		for (EndpointEvent ee : eeList) {
			String eventData = ee.getEventData().entrySet().stream()
					.map(e -> e.getKey() + "=" + e.getValue())
					.collect(Collectors.joining("&"));

			s += ee.getEventType() + "," + ee.getTimestamp() + "," + eventData + ",";
		}
		return s;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
