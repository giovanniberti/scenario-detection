package it.unifi.webapp.backend.model;

import com.google.gson.annotations.Expose;

import javax.management.j2ee.statistics.TimeStatistic;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "Video")
public class Video extends BaseEntity {

	@Expose
	private String name;

	@Transient
	private List<VideoComment> comments;

	@Expose
	@Transient
	private List<VideoLike> likes;

	@Expose
    @Transient
    private VideoRating rating;

	@Expose
	@ManyToOne
	private Channel channel;

	@Expose
	private Timestamp commitTime;

	//This is something like a payload for our purposes
	@Expose
	private String videoDescriptor;

	public Video() {
	}

	public Video(String uuid, String name, String videoDescriptor, Channel ch) {
		super(uuid);
		this.name = name;
		this.videoDescriptor = videoDescriptor;
		this.channel = ch;
		this.commitTime = new Timestamp(System.currentTimeMillis());
	}

	public String getName() {
		return name;
	}

	public Channel getChannel() {
		return this.channel;
	}

	public String getVideoDescriptor() {
		return videoDescriptor;
	}

	public Timestamp getCommitTime() {
		return commitTime;
	}
}
