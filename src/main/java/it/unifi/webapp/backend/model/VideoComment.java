package it.unifi.webapp.backend.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "VideoComment")
public class VideoComment extends BaseEntity {

	private Timestamp commitTime;

	@ManyToOne
	private User writer;

	@ManyToOne
	private Video video;

	private String commentText;

	public VideoComment() {
	}

	public VideoComment(String uuid, String text, User writer, Video video) {
		super(uuid);
		commentText = text;
		this.video = video;
		this.writer = writer;
		this.commitTime = new Timestamp(System.currentTimeMillis());
	}

	public Timestamp getCommitTime() {
		return commitTime;
	}

	public String getCommentText() {
		return commentText;
	}

	public User getWriter() {
		return writer;
	}
}
