package it.unifi.webapp.backend.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;


public class JsonResponseBuilder {

	private Gson gson;
	private JsonElement jsonElement;

	public JsonResponseBuilder() {
	}

	public void addField(String name, Object o) {
		this.jsonElement.getAsJsonObject().add(name, gson.toJsonTree(o, o.getClass()));
	}

	public void addField(String name, boolean o) {
		this.jsonElement.getAsJsonObject().addProperty(name, String.valueOf(o));
	}

	public void addField(String name, String o) {
		this.jsonElement.getAsJsonObject().addProperty(name, o);
	}

	public void addField(String name, int o) {
		this.jsonElement.getAsJsonObject().addProperty(name, o);
	}

	public String getJson() {
		return gson.toJson(jsonElement);
	}

	public void createResponse() {
		gson = new Gson();
		jsonElement = gson.toJsonTree(new Object());
	}
}
