package it.unifi.webapp.backend.initializer;

import it.unifi.webapp.backend.dao.UserDao;
import it.unifi.webapp.backend.model.User;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Startup
@Singleton
public class UnloggedUser {

	public static String uuid = "00000000-0000-0000-0000-000000000000";

	@Inject
	private UserDao userDao;

	@PostConstruct
	public void init() {
		User byUsername = userDao.findByUsername(uuid, uuid);
		if(byUsername==null){
			User unlogged = new User(uuid, uuid, uuid);
			userDao.save(unlogged);
		}
	}

}
