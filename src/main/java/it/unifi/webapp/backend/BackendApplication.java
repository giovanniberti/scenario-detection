package it.unifi.webapp.backend;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class BackendApplication extends Application {
}
