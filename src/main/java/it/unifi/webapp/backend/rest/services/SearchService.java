package it.unifi.webapp.backend.rest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import it.unifi.webapp.backend.dao.ChannelDao;
import it.unifi.webapp.backend.dao.VideoDao;
import it.unifi.webapp.backend.model.Channel;
import it.unifi.webapp.backend.model.utils.LogSystem;
import it.unifi.webapp.backend.model.utils.ObservedEvent;
import it.unifi.webapp.backend.model.Video;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Path("/services")
public class SearchService {

	@Inject
	private LogSystem logSystem;

	@Inject
	private VideoDao videoDao;

	@Inject
	private ChannelDao channelDao;

	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public String search(@QueryParam("query") @DefaultValue("") String query,
	                     @QueryParam("scenario") String scenario,
	                     @QueryParam("id") int id) {
		List<Video> videos = videoDao.getVideosFromKeyWord(query);
		Type videoListType = new TypeToken<ArrayList<Video>>() {}.getType();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		String videosJson = gson.toJson(videos, videoListType);

		String json = "{\n" +
				"\"videos\": " +
				videosJson + ",";

		List<Channel> channels = channelDao.getChannelsFromKeyWord(query);
		Type channelListType = new TypeToken<ArrayList<Video>>() {}.getType();
		String channelJson = gson.toJson(channels, channelListType);

		json += "\n" +
				"\"channels\": " +
				channelJson +
				"\n}";

		if (scenario != "" && id != 0) {
			logSystem.log(scenario, id, ObservedEvent.search());
		}
		return json;
	}

}
