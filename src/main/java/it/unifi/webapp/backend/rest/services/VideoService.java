package it.unifi.webapp.backend.rest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import it.unifi.webapp.backend.dao.*;
import it.unifi.webapp.backend.model.*;
import it.unifi.webapp.backend.dto.CommentDto;
import it.unifi.webapp.backend.dto.VideoDetailsDto;
import it.unifi.webapp.backend.mapper.CommentMapper;
import it.unifi.webapp.backend.model.utils.LogSystem;
import it.unifi.webapp.backend.model.utils.ObservedEvent;
import it.unifi.webapp.backend.utils.JsonResponseBuilder;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.*;

@Path("/services/videos")
public class VideoService {

	@Inject
	private LogSystem logSystem;

	@Inject
	private VideoDao videoDao;

	@Inject
	private ChannelDao channelDao;

	@Inject
	private JsonResponseBuilder jsBuilder;

	@Inject
	private UserDao usrDao;

	@Inject
	private VideoLikeDao videoLikeDao;

	@Inject
    private VideoRatingDao videoRatingDao;

	@Inject
	private VideoCommentDao videoCommentDao;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/watch")
	@Transactional
	public String watchVideo(@QueryParam("videoUUID") String videoUUID, @QueryParam("usrUUID") String usrUUID,
                             @QueryParam("scenario") String scenario, @QueryParam("id") int id) {
		String videoId = videoDao.findIdbyUUID(videoUUID, "Video");
		Video video = videoDao.findById(Long.parseLong(videoId));
		jsBuilder.createResponse();
		if (video != null) {
			if (usrUUID != null) {
				String usrID = usrDao.findIdbyUUID(usrUUID, "User");
				boolean isSubscribed = channelDao.findSubscriber(video.getChannel().getId().toString(), usrID);
				jsBuilder.addField("subscribe", isSubscribed);

				boolean isLiked = videoLikeDao.findLike(usrID, videoId);
				jsBuilder.addField("like", isLiked);

				Integer rating = videoRatingDao.findRating(usrID, videoId);
				jsBuilder.addField("rating", rating != null ? rating : -1);
			}
			if (scenario != "" && id != 0) {
				logSystem.log(scenario, id, ObservedEvent.viewVideo());
			}
			return jsBuilder.getJson();
		}
		jsBuilder.addField("status", "Error");

		return jsBuilder.getJson();
	}

	@GET
	@Path("/random")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public String randomVideo(@QueryParam("number") @DefaultValue("10") int number) {
		List<Video> results = videoDao.getOrderedRandomVideos(number);
		Type listType = new TypeToken<ArrayList<Video>>() {}.getType();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		String json = gson.toJson(results, listType);
		return json;
	}

	@GET
	@Path("/details")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response getVideoDetails(@QueryParam("videoUUID") String videoUUID) {
		String videoId = videoDao.findIdbyUUID(videoUUID, "Video");
		if( "".equals(videoId) ){
			return Response.status(Response.Status.NOT_FOUND).entity("Video not found").build();
		}
		Video video = videoDao.findById(Long.parseLong(videoId));
		List<VideoComment> videoComments = videoCommentDao.findVideoComments(Long.parseLong(videoId));
		int likes = videoLikeDao.countLike(Long.parseLong(videoId));

		VideoDetailsDto dto = new VideoDetailsDto();
		dto.setId(Long.parseLong(videoId));
		dto.setUserId(video.getChannel().getOwner().getId());
		dto.setOwnerName(video.getChannel().getOwner().getUsername());
		dto.setTitle(video.getName());
		dto.setLikes(Integer.toUnsignedLong(likes));
		dto.setCommitTime(video.getCommitTime().toString());
		List<CommentDto> cdto = new ArrayList<>();
		for(VideoComment vc:videoComments){
			CommentDto cd = new CommentMapper().modelToDto(vc,new CommentDto(),channelDao);
			cdto.add(cd);
		}
		dto.setComments(cdto);

		return Response.ok().entity(dto).build();
	}

	@GET
	@Path("/like")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public String likeVideo(@QueryParam("videoUUID") String videoUUID, @QueryParam("usrUUID") String usrUUID,
                            @QueryParam("scenario") String scenario, @QueryParam("id") int id) {
		String videoId = videoDao.findIdbyUUID(videoUUID, "Video");
		Video video = videoDao.findById(Long.parseLong(videoId));
		jsBuilder.createResponse();
		if (video != null) {
			if (usrUUID != null) {
				String usrID = usrDao.findIdbyUUID(usrUUID, "User");
				User usr = usrDao.findById(Long.parseLong(usrID));
				VideoLike vl = new VideoLike(UUID.randomUUID().toString(), usr, video);
				boolean exist = videoLikeDao.findLike(usrID, videoId);
				if (exist) {
					videoLikeDao.deleteLike(usrID, videoId);
					jsBuilder.addField("like", false);
					if (scenario != "" && id != 0) {
						logSystem.log(scenario, id, ObservedEvent.dislike());
					}
				} else {
					videoLikeDao.save(vl);
					jsBuilder.addField("like", true);
					if (scenario != "" && id != 0) {
						logSystem.log(scenario, id, ObservedEvent.like());
					}
				}
			}
			return jsBuilder.getJson();
		}
		jsBuilder.addField("status", "Error");
		return jsBuilder.getJson();
	}

    @GET
    @Path("/rate")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public String rateVideo(@QueryParam("videoUUID") String videoUUID, @QueryParam("usrUUID") String usrUUID,
                            @QueryParam("scenario") String scenario, @QueryParam("id") int id, @QueryParam("rating") Integer rating) {
        String videoId = videoDao.findIdbyUUID(videoUUID, "Video");
        Video video = videoDao.findById(Long.parseLong(videoId));
        jsBuilder.createResponse();
        if (video != null && rating != null) {
            if (usrUUID != null) {
                String usrID = usrDao.findIdbyUUID(usrUUID, "User");
                User usr = usrDao.findById(Long.parseLong(usrID));
                VideoRating vr = new VideoRating(UUID.randomUUID().toString(), usr, video, rating);
                boolean exist = videoRatingDao.findRating(usrID, videoId) != null;
                if (exist) {
                    videoRatingDao.deleteRating(usrID, videoId);
                }
                videoRatingDao.save(vr);
                jsBuilder.addField("rating", rating);
                if (scenario != "" && id != 0) {
                    logSystem.log(scenario, id, ObservedEvent.rating(rating));
                }
            }
            return jsBuilder.getJson();
        }
        jsBuilder.addField("status", "Error");
        return jsBuilder.getJson();
    }

    @GET
	@Path("/comment")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response commentVideo(@QueryParam("videoUUID") String videoUUID, @QueryParam("usrUUID") String usrUUID,
                                 @QueryParam("comment") String comment, @QueryParam("scenario") String scenario,
                                 @QueryParam("id") int id) {
		String videoId = videoDao.findIdbyUUID(videoUUID, "Video");
		Video video = videoDao.findById(Long.parseLong(videoId));
		if (video != null) {
			if (usrUUID != null) {
				String usrID = usrDao.findIdbyUUID(usrUUID, "User");
				User usr = usrDao.findById(Long.parseLong(usrID));
				VideoComment vc = new VideoComment(UUID.randomUUID().toString(), comment, usr, video);
				videoCommentDao.save(vc);
				if (scenario != "" && id != 0) {
					logSystem.log(scenario, id, ObservedEvent.comment());
				}
				CommentDto dto = new CommentMapper().modelToDto(vc, new CommentDto(),channelDao);
				return Response.ok().entity(dto).build();
			}
		}
		return Response.serverError().build();
	}

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/stats")
    @Transactional
    public String getStats(@QueryParam("videoUUID") String videoUUID, @QueryParam("scenario") String scenario, @QueryParam("id") int id) {
        Map<LocalDate, Integer> likesHistogram = videoLikeDao.histogramLikes(videoUUID);

		if (!scenario.equals("") && id != 0) {
			logSystem.log(scenario, id, ObservedEvent.stats());
		}

        jsBuilder.createResponse();
        jsBuilder.addField("likesHistogram", likesHistogram);
        return jsBuilder.getJson();
    }
}
