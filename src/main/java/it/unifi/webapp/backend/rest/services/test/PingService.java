package it.unifi.webapp.backend.rest.services.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/services/ping")
public class PingService {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String ping() {
		return "Ciao";
	}
}
