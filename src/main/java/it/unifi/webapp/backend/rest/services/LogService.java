package it.unifi.webapp.backend.rest.services;


import it.unifi.webapp.backend.dao.UserDao;
import it.unifi.webapp.backend.initializer.UnloggedUser;
import it.unifi.webapp.backend.model.utils.LogSystem;
import it.unifi.webapp.backend.model.utils.SessionScenario;
import it.unifi.webapp.backend.model.User;
import it.unifi.webapp.backend.utils.JsonResponseBuilder;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Path("services/log")
public class LogService {

	private static Integer nextId = 1;

	@Inject
	private UserDao usrDao;

	@Inject
	JsonResponseBuilder jsBuilder;

	@Inject
	LogSystem ls;

	public void getData(@QueryParam("data") String data, @QueryParam("scenario") String scenario) {
		try {
			FileWriter fw = new FileWriter("/opt/scenedet-results" + "/" + scenario + ".csv", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			out.println(data);
			System.out.println(data);
			out.close();
		} catch (IOException e) {}
	}

	@GET
	@Path("/ID")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public String getID() {
		jsBuilder.createResponse();
		int id;
		synchronized (nextId) {
			id = nextId++;
		}
		jsBuilder.addField("ID", id);
		return jsBuilder.getJson();
	}

	@GET
	@Path("/startScenario")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public Response startScenarioSession(@QueryParam("scenario") String scenario, @QueryParam("id") int id,
	                                     @QueryParam("usrUUID") @DefaultValue("") String usrUUID) {
		if (scenario != "" && id != 0) {
			User usr = null;
			if( !"".equals(usrUUID) ) {
				String usrId = usrDao.findIdbyUUID(usrUUID, "User");
				usr = usrDao.findById(Long.parseLong(usrId));
			} else {
				String usrId = usrDao.findIdbyUUID(UnloggedUser.uuid, "User");
				usr = usrDao.findById(Long.parseLong(usrId));
			}
			ls.addSessionScenario(new SessionScenario(scenario, id, usr));
			System.out.println("User " + usr.getUuid() + " started scenario: " + scenario + " for session id: " + id);
			return Response.ok().build();
		}
		return Response.status(400).build();
	}

	@GET
	@Path("/endScenario")
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public Response endScenarioSession(@QueryParam("scenario") String scenario, @QueryParam("id") int id) {
		if (scenario != null && id != 0) {
			System.out.println("Finished scenario: " + scenario + " for session id: " + id);
			boolean ended = ls.endScenario(scenario, id);
			if(ended)
				return Response.ok().build();
			else
				return Response.status(Response.Status.BAD_REQUEST).entity("Scenario vuoto.").build();
		}
		return Response.serverError().build();
	}
}
