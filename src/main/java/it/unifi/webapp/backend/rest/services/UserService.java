package it.unifi.webapp.backend.rest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import it.unifi.webapp.backend.dao.ChannelDao;
import it.unifi.webapp.backend.dao.SubscriberDao;
import it.unifi.webapp.backend.dao.UserDao;
import it.unifi.webapp.backend.model.Channel;
import it.unifi.webapp.backend.model.utils.LogSystem;
import it.unifi.webapp.backend.model.utils.ObservedEvent;
import it.unifi.webapp.backend.model.User;
import it.unifi.webapp.backend.utils.JsonResponseBuilder;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Path("/services/users")
public class UserService {

	@Inject
	private UserDao usrDao;

	@Inject
	private ChannelDao chDao;

	@Inject
	private JsonResponseBuilder responseBuilder;

	@Inject
	private LogSystem logSystem;

	@Inject
	private SubscriberDao subDao;

	@Path("/signup")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Transactional
	public Response signUP(@QueryParam("username") String username, @QueryParam("psw") String psw) {
		if (username != null && psw != null) {
			if( usrDao.usedUsername(username) )
				return Response.serverError().entity("Duplicate username.").build();
			User user = new User(UUID.randomUUID().toString(), username, psw);
			Channel ch = new Channel(UUID.randomUUID().toString(), user);
			subDao.save(user);
			chDao.save(ch);
			return Response.ok().build();
		}
		return Response.serverError().build();
	}

	@Path("/login")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public String signIN(@QueryParam("username") String username, @QueryParam("psw") String psw, @QueryParam("scenario"
    ) String scenario, @QueryParam("id") int id) {
		User usr = usrDao.findByUsername(username, psw);
		responseBuilder.createResponse();
		if (usr != null) {
			responseBuilder.addField("username", username);
			responseBuilder.addField("uuid", usr.getUuid());
			String chUUID = chDao.getFromUserid(usr.getId()).getUuid();
			responseBuilder.addField("chUUID", chUUID);
			responseBuilder.addField("status", true);
			if (scenario != "" && id != 0) {
				logSystem.log(scenario, id, ObservedEvent.login());
			}
		} else {
			responseBuilder.addField("status", false);
		}
		return responseBuilder.getJson();
	}

	@Path("/logout")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response logout(@QueryParam("scenario") String scenario, @QueryParam("id") int id) {
		if (id != 0) {
			logSystem.log(scenario, id, ObservedEvent.logout());
			return Response.ok().build();
		}
		return Response.status(400).build();
	}

	@Path("/followedChannels")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public String getFollowedChannels(@QueryParam("usrUUID") String usrUUID) {
		String usrId = usrDao.findIdbyUUID(usrUUID, "User");
		List<BigInteger> channelIds = chDao.getFollowedChannels(usrId);
		List<Channel> channels = new ArrayList<>();
		for (BigInteger ID : channelIds) {
			channels.add(chDao.findById(Long.parseLong(ID.toString())));
		}
		Type listType = new TypeToken<ArrayList<Channel>>() {
		}.getType();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		String json = gson.toJson(channels, listType);
		return json;
	}

}
