package it.unifi.webapp.backend.rest.services;

import it.unifi.webapp.backend.utils.ZipUtility;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("/services/dataset")
public class DatasetService {

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getDataset(){
		System.out.println("Invoked getDataset");
		ZipUtility zip = new ZipUtility();
		File folder = new File("/opt/scenedet-results");
		List<File> list = new ArrayList<>();
		list.add(folder);
		try {
			String zipPath = System.getProperty("jboss.server.data.dir") + "/dataset.zip";
			zip.zip(list, zipPath );
			File zipped = new File(zipPath);
			return Response.ok(zipped, MediaType.APPLICATION_OCTET_STREAM)
			        .header("Content-Disposition", "attachment; filename=\"" + zipped.getName() + "\"" )
			        .build();
		} catch (IOException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
	}

}
