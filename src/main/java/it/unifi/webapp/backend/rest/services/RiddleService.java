package it.unifi.webapp.backend.rest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mysql.cj.util.StringUtils;
import it.unifi.webapp.backend.dao.RiddleDao;
import it.unifi.webapp.backend.model.Video;
import it.unifi.webapp.backend.model.utils.Riddle;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Path("/services/riddles")
public class RiddleService {

	@Inject
	private RiddleDao riddleDao;

	@GET
	@Path("/getall")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response getAllRiddles(){
		List<Riddle> allRiddles = riddleDao.getAll();
		Type listType = new TypeToken<ArrayList<Riddle>>() {}.getType();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		String json = gson.toJson(allRiddles, listType);
		return Response.ok().entity(json).build();
	}

	@GET
	@Path("/new")
	@Transactional
	public Response createRiddle(@QueryParam("riddle") String riddle,
	                             @QueryParam("solution") String solution) {
		if( StringUtils.isEmptyOrWhitespaceOnly(riddle) || StringUtils.isEmptyOrWhitespaceOnly(solution) )
			return Response.status(Response.Status.BAD_REQUEST).build();
		Riddle riddle1 = new Riddle(riddle, solution);
		riddleDao.save(riddle1);
		return Response.ok().build();
	}

}
