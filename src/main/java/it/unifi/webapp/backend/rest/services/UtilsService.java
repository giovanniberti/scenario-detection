package it.unifi.webapp.backend.rest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import it.unifi.webapp.backend.dao.ScenarioCounterDao;
import it.unifi.webapp.backend.dao.UserDao;
import it.unifi.webapp.backend.dao.VideoCommentDao;
import it.unifi.webapp.backend.dao.VideoDao;
import it.unifi.webapp.backend.dto.DbInfoDto;
import it.unifi.webapp.backend.model.utils.ScenarioCounter;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Path("/services/utils")
public class UtilsService {

	@Inject
	private VideoDao videoDao;

	@Inject
	private VideoCommentDao videoCommentDao;

	@Inject
	private UserDao userDao;

	@Inject
	private ScenarioCounterDao scenarioCounterDao;

	@GET
	@Path("/dbinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDbInfo(){
		DbInfoDto dto = new DbInfoDto();
		dto.setVideos(videoDao.getCount());
		dto.setComments(videoCommentDao.getCount());
		dto.setUsers(userDao.getCount());
		return Response.status(Response.Status.OK).entity(dto).build();
	}

	@GET
	@Path("/usersscenario")
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response getUserScenarios(@QueryParam("secret") String secret) {
		if( !"286618ab-7ec8-4e6c-b3a0-6fa6597d6b07".equals(secret) )
			return Response.status(Response.Status.UNAUTHORIZED).build();
		List<ScenarioCounter> all = scenarioCounterDao.getAll();
		Type scenarioCounterType = new TypeToken<ArrayList<ScenarioCounter>>() {}.getType();
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
		String json = gson.toJson(all, scenarioCounterType);
		return Response.ok().entity(json).build();
	}

}
