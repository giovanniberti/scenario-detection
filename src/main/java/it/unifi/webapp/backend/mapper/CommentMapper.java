package it.unifi.webapp.backend.mapper;

import it.unifi.webapp.backend.dao.ChannelDao;
import it.unifi.webapp.backend.model.Channel;
import it.unifi.webapp.backend.model.VideoComment;
import it.unifi.webapp.backend.dto.CommentDto;

public class CommentMapper {

	public CommentDto modelToDto(VideoComment vc, CommentDto dto, ChannelDao channelDao){
		dto.setCommitTime(vc.getCommitTime().toString());
		dto.setWriter(vc.getWriter().getUsername());
		dto.setWriterId(vc.getWriter().getId());
		dto.setWriterUuid(vc.getWriter().getUuid());

		Channel fromUserid = channelDao.getFromUserid(vc.getWriter().getId());
		System.out.println("Channel " + fromUserid);
		String uuid = fromUserid.getUuid();
		System.out.println(uuid);
		dto.setWriterChannelUuid(uuid);

		dto.setComment(vc.getCommentText());
		return dto;
	}

}
