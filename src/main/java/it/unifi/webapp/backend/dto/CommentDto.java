package it.unifi.webapp.backend.dto;

public class CommentDto {

	private String commitTime;
	private String writer;
	private Long writerId;
	private String writerUuid;
	private String writerChannelUuid;
	private String comment;

	public String getCommitTime() {
		return commitTime;
	}

	public void setCommitTime(String commitTime) {
		this.commitTime = commitTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Long getWriterId() {
		return writerId;
	}

	public void setWriterId(Long writerId) {
		this.writerId = writerId;
	}

	public String getWriterUuid() {
		return writerUuid;
	}

	public void setWriterUuid(String writerUuid) {
		this.writerUuid = writerUuid;
	}

	public String getWriterChannelUuid() {
		return writerChannelUuid;
	}

	public void setWriterChannelUuid(String writerChannelUuid) {
		this.writerChannelUuid = writerChannelUuid;
	}
}
