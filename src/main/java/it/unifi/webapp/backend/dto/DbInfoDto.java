package it.unifi.webapp.backend.dto;

public class DbInfoDto {

	private Long videos;
	private Long comments;
	private Long users;

	public Long getVideos() {
		return videos;
	}

	public void setVideos(Long videos) {
		this.videos = videos;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public Long getUsers() {
		return users;
	}

	public void setUsers(Long users) {
		this.users = users;
	}
}
