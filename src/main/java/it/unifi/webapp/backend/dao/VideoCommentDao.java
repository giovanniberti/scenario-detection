package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.Video;
import it.unifi.webapp.backend.model.VideoComment;

import java.util.List;

public class VideoCommentDao extends BaseDao<VideoComment> {

	private static final long serialVersionUID = -1525986591245420336L;

	public VideoCommentDao() {
		super(VideoComment.class);
	}

	public void save(VideoComment comment) {
		if (comment.getId() != null) {
			super.update(comment);
		} else {
			super.save(comment);
		}
	}

	public List<VideoComment> findVideoComments(long videoId) {
		List<VideoComment> result = entityManager
				.createQuery("from VideoComment "
						             + "where video_id like :videoId", VideoComment.class)
				.setParameter("videoId", videoId)
				.getResultList();
		return result;
	}

	public Long getCount() {
		return entityManager.createQuery("SELECT count(*) from VideoComment ", Long.class).getSingleResult();
	}

}

