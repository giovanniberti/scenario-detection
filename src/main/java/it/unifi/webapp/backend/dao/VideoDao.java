package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.Video;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class VideoDao extends BaseDao<Video> {

	private static final long serialVersionUID = -2001907877579060481L;

	public VideoDao() {
		super(Video.class);
	}

	public void save(Video video) {
		if (video.getId() != null) {
			super.update(video);
		} else {
			super.save(video);
		}
	}

	public Long getCount() {
		return entityManager.createQuery("SELECT count(*) from Video ", Long.class).getSingleResult();
	}

	public List<Video> getVideosFromKeyWord(String key) {
		List<Video> result = entityManager
				.createQuery("from Video "
						             + "where name like '%" + key + "%'", Video.class)
				.getResultList();
		result.sort(new Comparator<Video>() {
			@Override
			public int compare(Video o1, Video o2) {
				return -o1.getCommitTime().compareTo(o2.getCommitTime());
			}
		});
		return result;
	}

	public List<Video> getOrderedRandomVideos(int number) {
		List<Video> result = entityManager
				.createQuery("from Video", Video.class)
				.getResultList();
		List<Video> random = new ArrayList<>();
		for( int i = 0; i < number && result.size()>0 ; i++ ){
			int nextInt = new Random().nextInt(result.size());
			random.add(result.get(nextInt));
			result.remove(nextInt);
		}
		random.sort(new Comparator<Video>() {
			@Override
			public int compare(Video o1, Video o2) {
				return -o1.getCommitTime().compareTo(o2.getCommitTime());
			}
		});
		return random;
	}

	public List<Video> getVideosFromChannelId(Long id) {
		List<Video> results = entityManager
				.createQuery("from Video where channel_id=:id", Video.class)
				.setParameter("id", id)
				.getResultList();
		results.sort(new Comparator<Video>() {
			@Override
			public int compare(Video o1, Video o2) {
				return -o1.getCommitTime().compareTo(o2.getCommitTime());
			}
		});
		return results;
	}
}
