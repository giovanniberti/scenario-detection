package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.Video;
import it.unifi.webapp.backend.model.VideoRating;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class VideoRatingDao extends BaseDao<VideoRating> {

    private static final long serialVersionUID = -3663990275909408368L;

    public VideoRatingDao() {
        super(VideoRating.class);
    }

    public void save(VideoRating vl) {
        if (vl.getId() != null) {
            super.update(vl);
        } else {
            super.save(vl);
        }
    }

    public Integer findRating(String raterID, String videoID) {
        System.out.println("Searching for rating of video " + videoID + " for user " + raterID);
        TypedQuery<VideoRating> q = entityManager.createQuery("FROM VideoRating WHERE rater_id=:raterID AND " +
                "video_id=:videoID", VideoRating.class)
                .setParameter("raterID", raterID)
                .setParameter("videoID", videoID);
        try {
            VideoRating r = q.getSingleResult();
            System.out.println("Found rating");
            return r.getRating();
        } catch (NoResultException nre) {
            System.out.println("Couldn't find rating");
            return null;
        }
    }

    public int countRating(long videoId) {
        List<VideoRating> result = entityManager
                .createQuery("from VideoRating "
                        + "where video_id like :videoId", VideoRating.class)
                .setParameter("videoId", videoId)
                .getResultList();
        return result.size();
    }

    public boolean deleteRating(String raterID, String videoID) {
        Query q = entityManager.createNativeQuery("DELETE FROM VideoRating WHERE rater_id=:raterID AND " +
                "video_id=:videoID")
                .setParameter("raterID", raterID)
                .setParameter("videoID", videoID);
        try {
            q.executeUpdate();
            return true;
        } catch (NoResultException nre) {
            return false;
        }
    }
}
