package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.Channel;
import it.unifi.webapp.backend.model.User;
import it.unifi.webapp.backend.model.Video;
import it.unifi.webapp.backend.model.VideoLike;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.time.*;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.TreeMap;

public class VideoLikeDao extends BaseDao<VideoLike> {

	private static final long serialVersionUID = -3663990275909408360L;

	public VideoLikeDao() {
		super(VideoLike.class);
	}

	public void save(VideoLike vl) {
		if (vl.getId() != null) {
			super.update(vl);
		} else {
			super.save(vl);
		}
	}

	public boolean findLike(String likerID, String videoID) {
		Query q = entityManager.createNativeQuery("SELECT * FROM VideoLike WHERE liker_id=:likerID AND " +
				                                          "video_id=:videoID")
		                       .setParameter("likerID", likerID)
		                       .setParameter("videoID", videoID);
		try {
			q.getSingleResult();
			return true;
		} catch (NoResultException nre) {
			return false;
		}
	}

	public int countLike(long videoId) {
		List<VideoLike> result = entityManager
				.createQuery("from VideoLike "
						             + "where video_id like :videoId", VideoLike.class)
				.setParameter("videoId", videoId)
				.getResultList();
		return result.size();
	}

	public boolean deleteLike(String likerID, String videoID) {
		Query q = entityManager.createNativeQuery("DELETE FROM VideoLike WHERE liker_id=:likerID AND " +
                                                          "video_id=:videoID")
		                       .setParameter("likerID", likerID)
		                       .setParameter("videoID", videoID);
		try {
			q.executeUpdate();
			return true;
		} catch (NoResultException nre) {
			return false;
		}
	}

	public TreeMap<LocalDate, Integer> histogramLikes(String videoUUID) {
		String videoId = findIdbyUUID(videoUUID, "Video");

		/* XXX
		 * This is done only for experimentation purposes in order to stress the DB.
		 * The following queries can be deleted without impacting the application's functionality.
		 */
		System.out.println("#### videoId: " + videoId);
		Video v = entityManager.createQuery("from Video where id = :videoId", Video.class)
				.setParameter("videoId", Long.parseLong(videoId))
				.getSingleResult();
		Channel channel = v.getChannel();

		for (User u : channel.getSubscribers()) {
			entityManager.createNativeQuery("select v.* from Video v " +
					"join Channel c on v.channel_id = c.id " +
					"where c.owner_id = :userId")
					.setParameter("userId", u.getId())
					.getResultList();
		}
		/* XXX */

		List<Object[]> result = (List<Object[]>) entityManager
                .createNativeQuery("SELECT EXTRACT(YEAR FROM commitTime) as like_year, EXTRACT(WEEK FROM commitTime) as like_week, count(*)" +
                        "from VideoLike " +
						"where video_id = :videoId " +
                        "group by like_year, like_week " +
                        "order by like_week asc")
				.setParameter("videoId", videoId)
                .getResultList();

        TreeMap<LocalDate, Integer> histogram = new TreeMap<>();
        for (Object[] row : result) {
            Integer year = (Integer) row[0];
            Integer week = (Integer) row[1];
            LocalDate localDate = LocalDate.now()
                    .with(Year.of(year))
                    .with(IsoFields.WEEK_OF_WEEK_BASED_YEAR, week)
                    .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));

            BigInteger count = (BigInteger) row[2];

            histogram.put(localDate, count.intValue());
        }

        return histogram;
    }
}
