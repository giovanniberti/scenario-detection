package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.Channel;
import it.unifi.webapp.backend.model.User;
import it.unifi.webapp.backend.model.Video;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ChannelDao extends BaseDao<Channel> {

	private static final long serialVersionUID = 8018773460992187876L;

	public ChannelDao() {
		super(Channel.class);
	}

	public void save(Channel ch) {
		if (ch.getId() != null) {
			super.update(ch);
		} else {
			super.save(ch);
		}
	}

	public Channel get(String chUUID) {
		Channel result = entityManager
				.createQuery("from Channel "
						             + "where uuid = :uid", Channel.class)
				.setParameter("uid", chUUID)
				.getSingleResult();
		return result;
	}

	public Channel getFromUserid(Long UserId) {
		return entityManager
				.createQuery("from Channel "
						             + "where owner_id = :usrId", Channel.class)
				.setParameter("usrId", UserId)
				.getSingleResult();
	}

	public List<Channel> getChannelsFromKeyWord(String key) {
		List<Channel> result = entityManager
				.createQuery(" select c from Channel c left join c.owner u " +
						             " where u.username like '%" + key + "%'", Channel.class)
				.getResultList();
		return result;
	}

	public List<Channel> getAllChannels() {
		List<Channel> result = entityManager
				.createQuery(" select c from Channel c, User u where c.owner.id = u.id ", Channel.class)
				.getResultList();
		return result;
	}

	public boolean findSubscriber(String chID, String usrID) {
		Query q = entityManager.createNativeQuery("SELECT * FROM Channel_User WHERE Channel_id=:chID AND " +
                                                          "subscribers_id=:usrID")
		                       .setParameter("chID", chID)
		                       .setParameter("usrID", usrID);
		try {
			q.getSingleResult();
			return true;
		} catch (NoResultException nre) {
			return false;
		}
	}

	public List<BigInteger> getSubscribers(String chID) {
		try {
			return this.entityManager.createNativeQuery(
					" SELECT subscribers_id FROM Channel_User WHERE Channel_id = :chID ")
			                         .setParameter("chID", chID)
			                         .getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}

	public List<BigInteger> getFollowedChannels(String usrID) {
		Query q = entityManager.createNativeQuery("SELECT Channel_id FROM Channel_User WHERE subscribers_id=:usrID")
		                       .setParameter("usrID", usrID);
		try {
			@SuppressWarnings("unchecked")
			List<BigInteger> result = q.getResultList();
			return result;
		} catch (NoResultException nre) {
			return null;
		}
	}

}
