package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.utils.ScenarioCounter;
import it.unifi.webapp.backend.model.User;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

public class ScenarioCounterDao extends BaseDao<ScenarioCounter> {

	public ScenarioCounterDao() {
		super(ScenarioCounter.class);
	}

	public ScenarioCounter find(User user, String scenario) {
		try {
			return this.entityManager.createQuery("from ScenarioCounter " +
					                                      "where userScenario.user = :usr and userScenario.scenario = :scene ",
			                                      ScenarioCounter.class)
			                         .setParameter("usr", user)
			                         .setParameter("scene", scenario != null ? scenario : "")
			                         .getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<ScenarioCounter> findByUser(User user) {
		try {
			return this.entityManager.createQuery("from ScenarioCounter " +
					                                      "where userScenario.user = :usr", ScenarioCounter.class)
			                         .setParameter("usr", user)
			                         .getResultList();
		} catch (NoResultException e) {
			return null;
		}
	}

	public ScenarioCounter findByScenario(String scenario) {
		try {
			return this.entityManager.createQuery("from ScenarioCounter " +
					                                      "where userScenario.scenario = :scene ", ScenarioCounter.class)
			                         .setParameter("scene", scenario != null ? scenario : "")
			                         .getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<ScenarioCounter> getAll() {
		try {
			return this.entityManager.createQuery("from ScenarioCounter order by userScenario.user.username, " +
					                                      "userScenario" +
					                                      ".scenario", ScenarioCounter.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}
}
