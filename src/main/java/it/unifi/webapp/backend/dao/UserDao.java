package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.User;

public class UserDao extends BaseDao<User> {

	private static final long serialVersionUID = 3399780086555063363L;

	public UserDao() {
		super(User.class);
	}

	public boolean login(User user) {
		if (user.getUsername() != null) {
			return (findByUsername(user.getUsername(), user.getPsw()) != null);
		}
		return false;
	}

	public boolean usedUsername(String username) {
		try {
			User result = entityManager
					.createQuery("from User "
							             + "where username = :username", User.class)
					.setParameter("username", username)
					.getSingleResult();
			if( result != null )
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public User findByUsername(String username, String psw) {
		try {
			User result = entityManager
					.createQuery("from User "
							             + "where username = :username and psw = :psw", User.class)
					.setParameter("username", username)
					.setParameter("psw", psw)
					.getSingleResult();
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	public Long getCount() {
		return entityManager.createQuery("SELECT count(*) from User ", Long.class).getSingleResult();
	}

}
