package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.User;

public class SubscriberDao extends BaseDao<User> {

	private static final long serialVersionUID = 5633768464506788409L;

	public SubscriberDao() {
		super(User.class);
	}

	public void save(User user) {
		if (user.getId() != null) {
			super.update(user);
		} else {
			super.save(user);
		}
	}
}
