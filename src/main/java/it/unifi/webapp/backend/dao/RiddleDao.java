package it.unifi.webapp.backend.dao;

import it.unifi.webapp.backend.model.utils.Riddle;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

public class RiddleDao extends BaseDao<Riddle> {

	public RiddleDao() {
		super(Riddle.class);
	}

	public List<Riddle> getAll() {
		try {
			return this.entityManager.createQuery("SELECT r FROM Riddle r ORDER BY r.commitTime")
			                         .getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		}
	}

}
